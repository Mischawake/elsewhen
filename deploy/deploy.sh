#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Usage: deploy.sh ip/url"
    exit
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SSH_HOST=elw@"$1"
SSH_ARGS="-i ./deploy/auth/machine -p 5022"

cd $DIR/..

BUILD_DIR=build
TAR=bundle.tar.gz

echo "* Bundling project"
tar -zcvf $TAR deploy/data/elsewhen.service .eslintrc.json build-server.js package.json webpack.config.js app data server

echo "* Syncing to server"
rsync --progress -rave "ssh $SSH_ARGS" $TAR $SSH_HOST:/home/elw/$TAR
rm $TAR

CMDS="
rm -rf elsewhen;
mkdir elsewhen;
tar -zxf $TAR -C elsewhen;
rm $TAR;
echo \"* Setup swap space.\";
sudo fallocate -l 2G /swapfile;
sudo chmod 600 /swapfile;
sudo mkswap /swapfile;
sudo swapon /swapfile;
echo \"* Compiling project...\";
cd elsewhen;
npm install;
npm run build:client;
npm run build:server;
echo \"* Restarting service...\";
sudo systemctl stop elsewhen.service;
sudo cp deploy/data/elsewhen.service /etc/systemd/system/multi-user.target.wants;
sudo systemctl daemon-reload;
sudo systemctl start elsewhen.service;
echo \"* Cleanup swap.\";
sudo swapoff /swapfile;
sudo rm /swapfile;
echo \"* Build running.\";
"
ssh $SSH_ARGS $SSH_HOST $CMDS
