const _ = require('lodash');
const _$ = require('jquery');
const now = require('performance-now');

//
// Helpers
//
function clamp(v, min, max) {
    return Math.min(Math.max(v, min), max);
}

function lerp(t, a, b) {
    return a * (1.0 - t) + b * t;
}

function delerp(t, a, b) {
    return (t - a) / (b - a);
}

function remap(t, a, b, a1, b1) {
    let x = clamp(t, a, b);
    return lerp(delerp(x, a, b), a1, b1);
}

//
// Stats
//
class StatsView {
    constructor() {
        this.stats = {};
    }

    update(stats) {
        if (!_.isEqual(this.stats, stats)) {
            this.stats = _.cloneDeep(stats);
            this.render();
        }
    }

    render() {
        _$('#stats').empty();

        let html = "<table>";
       

        for (let name in this.stats) {
            if (this.stats.hasOwnProperty(name)) {
                
                let value = this.stats[name];
                
                html += "<tr>";
                html += "<td>"+name+"</td><td>"+value+"</td>";
                html += "</tr>";

                // let div = _$('<div/>', {
                //     'class': 'stats-item'
                // });
                // div.append(_$('<div/>', {
                //     html: name
                // }));
                // div.append(_$('<div/>', {
                //     html: value
                // }));

                //_$('#stats').append(div);
            }
        }

        html += "</table>";

        _$('#stats').append(html);


    }
}

function GetStat(engine, name) {
    return engine.variablesState.$(name);
}

function SetStat(engine, name, value) {
    engine.variableState.$(name, value);
}

function FormatTimeStat(value) {
    let pm = value >= 12;
    if (value > 12) { value -= 12; }
    return value + ':00 ' + (pm ? 'pm' : 'am');
}

//
// Game
//
class Game {
    constructor(sendFn) {
        this.send = sendFn;
        this.playerId = 0;

        this.stats = {};
        this.statsView = new StatsView();

        this.choiceDur = 0;
        this.choiceEnd = 0;

        this.queue = [];
        this.queueTimer = 0;


    }

    update(dt, time) {
        // Display items
        this.queueTimer -= dt;
        if (this.queueTimer <= 0 && this.queue.length > 0) {
            let item = this.queue.shift();
            item();
            this.queueTimer = 0.2;
        }


        _$('#player-id').text( (this.playerId == 0 ? "Demon" : "Boy" ) + " ("+this.playerId+")" );


        // Timer
        if (this.choiceDur > 0) {
            let t = 1 - clamp(
                (this.choiceEnd - time) / (this.choiceDur * 1000), 0, 1
            );
            let a = delerp(t, 0.2, 0.8);
            let e = _$('#choice-timer');
            e.width((t * 100) + '%');
            e.css('opacity', a);
        }

        //space between choices and story needs to be updated based on choice height
        _$("#story").css("padding-bottom", _$("#choices").height() + 30 );

        // Stats
        this.statsView.update(this.stats);
    }

    doScroll(){
        let end = document.body.scrollHeight;    
        let top = _$(window).height() + _$(window).scrollTop();

        if( end != top ){

            let duration = clamp( (end - top) * 10.0, 500.0, 3000.0 );

            let scroller = _$("html, body");
            scroller.stop().animate({scrollTop:end}, duration, 'swing', function() { 
                //console.log("scroll complete");
            });
        }
        
    }

    enqueue(fn) {
        if (fn instanceof Function) {
            this.queue.push(fn);
        } else {
            console.log('Invalid enqueue type', typeof fn);
        }
    }

    updateStory(story, active, timeout) {
        let storyContainer = _$('#story');
        let choiceContainer = _$('#choices');
        let delay = 0;

        const showAfterDelay = (elem) => {
            this.enqueue(() => {
                elem.addClass('show');
            });
        };

        const addImageElem = (image) => {

            _$("#scene").css('background-image', 'url(' + image + ')');
            // let img = _$('<img/>', {
            //     src: image
            // });
            // let p = _$('<p/>', {
            //     'class': 'new',
            // });
            // p.append(img);
            // storyContainer.append(p);
            // showAfterDelay(p);

        };

        const addTextElem = (text) => {

            let p = _$('<p/>', {
                'class': 'new',
                html: text
            });
            storyContainer.append(p);
            showAfterDelay(p);
        };

        // Fade old text
        storyContainer.find('p').not('.old').addClass('old');

        // Remove choices
        choiceContainer.find('p.choice').remove();

        // Add last choice description - turning off for now, only display results, not the choices
        if (story.lastChoiceText != '') {
            // let p = _$('<p/>', {
            //     'class': 'new choice last-choice',
            //     html: story.lastChoiceText
            // });
            // storyContainer.append(p);
            // showAfterDelay(p);
        }

        // Add image
        if (story.image != '') {
            addImageElem(story.image);
        }

        // Add text
        for (const text of story.text) {
            addTextElem(text);
        }

        // Add choices
        const onChoice = (e, choiceIndex) => {
            e.preventDefault();

            // @TODO: Disable hyperlinks on choices after selecting
            //container.find('p.choice').remove();

            this.send('choose', {
                index: choiceIndex
            });
        };

        //choice formatting

        if (active) {
            for (const choice of story.choices) {
                let p = _$('<p/>', {
                    'class': 'choice new clickable',
                    text: choice.text,
                    click: (event) => {
                        onChoice(event, choice.index);
                    }
                });
                // let a = _$('<a/>', {
                //     text: choice.text,
                //     click: (event) => {
                //         onChoice(event, choice.index);
                //     }
                // });
                // p.append(a);
                choiceContainer.append(p);
                showAfterDelay(p);
            }
        } else {
            let p = _$('<p/>', {
                'class': 'choice new inactive',
                html: 'Waiting for ' + (this.playerId == 0 ? "Boy" : "Demon" ) + ' to act...'
            });
            choiceContainer.append(p);
            showAfterDelay(p);
        }

        this.choiceDur = timeout;
        this.choiceEnd = now() + timeout * 1000;

        // Stats
        for (const stat of story.stats) {
            this.stats[stat.name] = stat.value;
        }

        if (active) {
            console.log('It\'s our turn to choose');
        } else {
            console.log('Waiting for the other player to choose...');
        }

        this.doScroll();
    }
}

//
// App
//
const AppMode = {
    None: 0,
    Title: 1,
    Waiting: 2,
    Playing: 3,
    LostConn: 4
};

class App {
    constructor() {

        this.game = new Game((t, d) => {
            this.sendMessage(t, d);
        });

        _$('#start-btn').click(() => {
            this.sendMessage('startNow', {});
        });

        _$('#ready-btn').click(() => {

            this.setMode(AppMode.Waiting);
            this.connect();
        });

        this.mode = AppMode.None;
        this.setMode(AppMode.Title);

        this.prevTime = 0;
        requestAnimationFrame((t) => this.update());
    }



    connect() {
        var host = window.document.location.host;
        this.ws = new WebSocket('ws://' + host + '/play');

        this.ws.onerror = (error) => {
            console.log('socket error', error);
        };

        this.ws.onopen = () => {
            console.log('Sending join');
            this.sendMessage('ready', {});
        };

        this.ws.onmessage = (e) => {
            let decode = function(data) {
                if (typeof(data) === 'string') {
                    return JSON.parse(data);
                } else {
                    return {
                        type: 'unknown type: ' + typeof(data),
                        data: data
                    };
                }
            };
            let msg = decode(e.data);
            this.onMessage(msg.type, msg.data);
        };

        this.ws.onclose = () => {
            console.log('socket closed');
            this.setMode(AppMode.LostConn);
        };
    }

    sendMessage(type, data) {
        let msg = JSON.stringify({
            type: type,
            data: data
        });
        this.ws.send(msg, (err) => {
            if (err !== undefined) {
                console.log('Send error', err);
            }
        });
    }

    onMessage(type, data) {
        switch (type) {
        case 'joined':
            console.log('Connected as player', data.playerId);
            this.game.playerId = data.playerId;
            break;
        case 'updateStory':
            this.setMode(AppMode.Playing);

            this.game.updateStory(
                data.story,
                data.playerId == this.game.playerId,
                data.timeout
            );
            break;
        default:
            console.log('Unknown message', type, data);
        }
    }

    update() {
        let time = now();
        let dt = (time - this.prevTime) / 1000.0;
        this.prevTime = time;
        requestAnimationFrame((t) => this.update(t));

        this.game.update(dt, time);
    }

    setMode(mode) {

        console.log(mode);

        if (this.mode != mode) {
            this.mode = mode;

            _$('#waiting').toggle(mode == AppMode.Waiting);
            _$('#lost-conn').toggle(mode == AppMode.LostConn);
            _$('#content').toggle(mode == AppMode.Playing);

            _$('#title-btns').toggle(mode == AppMode.Waiting || mode == AppMode.Title );
            _$('#instructions').toggle(mode == AppMode.Title );
            _$('#status').toggle(mode == AppMode.Waiting || mode == AppMode.LostConn );
            _$('#ready-btn').toggle(mode == AppMode.Title );
            _$('#start-btn').toggle( false );//mode == AppMode.Waiting);
           // _$('#spacer').toggle(mode == AppMode.Playing);
           // _$('#story').toggle(mode == AppMode.Playing);
            // _$('#story-wrapper').toggle(mode == AppMode.Playing);
            // _$('#choices').toggle(mode == AppMode.Playing);
            // _$('#choice-timer-outer').toggle(mode == AppMode.Playing);
            // _$('#footer').toggle(mode == AppMode.Playing);
        }
    }
}

function run() {
    let app = new App();
    //app.connect();

    //document.body.requestFullscreen();
    window.scrollTo(0, 1);

}

window.addEventListener('load', e => { run(); });

_$(window).scroll(function(e) {

    if (_$("body, html").is(':animated')) {
        //console.log('scroll happen by animate');
    } else {
        // scroll happen by call
        //console.log('scroll happen by call');
        _$("body, html").stop();
    }

});


