const _ = require('lodash');
const assert = require('assert');
const now = require('performance-now');
const Story = require('../data/story.js');
const Inkjs = require('./deps/ink.js');

const kTickRate = 20;

//
// Helpers
//
function getEngineStat(engine, name) {
    return engine.variablesState.$(name);
}

function setEngineStat(engine, name, value) {
    engine.variablesState.$(name, value);
}

function formatTimeStat(value) {
    let pm = value >= 12;
    if (value > 12) { value -= 12; }
    return value + ':00 ' + (pm ? 'pm' : 'am');
}

//
// Player
//
class Player {
    constructor(game, ws, id) {
        this.id = id;
        this.ws = ws;
        this.heartbeat = now();
        this.ready = false;
        this.bot = false;

        ws.on('message', (data) => {
            this.heartbeat = now();

            let recvMsg = (m) => {
                if (typeof(m) === 'string') {
                    return JSON.parse(m);
                } else {
                    return {
                        type: 'unknown type: ' + typeof(data),
                        data: data
                    };
                }
            };

            let msg = recvMsg(data);
            game.onMessage(this, msg.type, msg.data);
        });

        ws.on('error', (err) => {
            console.log('Error', err);
        });

        ws.on('close', () => {
            game.removePlayer(this);
            this.ws = null;
        });
    }

    sendMessage(type, data) {
        if( this.bot ) return;
        let msg = JSON.stringify({
            type: type,
            data: data
        });
        this.ws.send(msg, (err) => {
            if (err !== undefined) {
                console.log('Send error', err);
            }
        });
    }
}

//
// Game
//
const MAX_PLAYERS = 2;
const CHOICE_TIMEOUT = 10; 

class Game {
    constructor(id) {
        this.id = id;
        this.engine = new Inkjs.Story(Story.Content);
        this.players = [];
        this.started = false;
        this.finished = false;

        this.curPlayerIdx = 0; 
        this.choiceTimer = 0;
        this.choices = [];
        this.lastChoiceText = '';
    }

    onConnection(ws) {
        assert(this.waitingForPlayers());
        let id = this.players.length;
        let p = new Player(this, ws, id);
        this.players.push(p);
        console.log('Player joined', this.id, p.id);
    }

    onMessage(player, type, data) {
        switch (type) {
        case 'ready':
            console.log('Player ready', this.id, player.id);
            player.ready = true;

            player.sendMessage('joined', {
                playerId: player.id
            });

            if (_.sumBy(this.players, (x) => {
                return x.ready ? 1 : 0;
            }) == MAX_PLAYERS) {
                this.startGame();
            }
            break;
        case 'startNow':
            this.startGame();
            break;
        case 'choose':
            if (player.id == this.players[this.curPlayerIdx].id) {
                this.makeChoice(data.index);
            } else {
                console.log('Out of turn', this.id, player.id, data.index);
            }
            break;
        default:
            console.log('Unknown message', this.id, player.id, type, data);
        }
    }

    removePlayer(player) {
        console.log('Player left', this.id, player.id);

        //this.players.splice(this.players.indexOf(player), 1);
        player.bot = true; // turn player into bot

        // if (this.started) {
        //     this.curPlayerIdx = 0;
        //     this.choiceTimer = 0;
        // }

        let realPlayers = 0;
        for( let i = 0; i < this.players.length; i++ ){
            if( !this.players[i].bot ) realPlayers++;
        }

        if (realPlayers == 0 || this.players.length == 0) {
            this.finished = true;
        }
    }

    waitingForPlayers() {
        return !this.started && this.players.length < MAX_PLAYERS;
    }

    isFinished() {
        return this.finished;
    }

    broadcast(type, data) {
        for (let i = 0; i < this.players.length; i++) {
            let p = this.players[i];
            p.sendMessage(type, data);
        }
    }

    startGame() {
        if (!this.started) {
            console.log('Starting game', this.id);
            this.started = true;
            this.continueStory = true;
        }
    }

    makeChoice(idx) {
        let choice = _.find(this.choices, (x) => {
            return x.index == idx;
        });
        if (choice !== undefined) {
            console.log('Made choice', this.id, choice.index);

            this.lastChoiceText = choice.text;
            this.engine.ChooseChoiceIndex(choice.index);
            this.continueStory = true;
            this.choices = [];

        } 
        else {
            console.log('Invalid choice idx', this.id, idx,
                        _.map(this.choices, (x) => { return x.index; } ));
        }
    }

    update(dt) {
        if (!this.started || this.isFinished()) {
            return;
        }

        if (this.continueStory) {
            this.continueStory = false;
            this.runStory();
        }

   
        // Auto-choose if the timer expires // turned off for dev 
        this.choiceTimer += dt;

        if(this.choiceTimer > 5.0 && this.players[this.curPlayerIdx].bot ){
            let idx = Math.floor(Math.random() * this.choices.length);
            this.makeChoice(this.choices[idx].index);
        }

        // if (this.choiceTimer > CHOICE_TIMEOUT ) {
        //     console.log('Choice timer expired', this.id, this.curPlayerIdx);

        //     let idx = Math.floor(Math.random() * this.choices.length);
        //     this.makeChoice(this.choices[idx].index);
        // }

       
    }

    runStory() {
  
        setEngineStat(this.engine, 'prevPlayerId', this.curPlayerIdx );
        this.curPlayerIdx++;   
        this.curPlayerIdx = this.curPlayerIdx % this.players.length;
        setEngineStat(this.engine, 'curPlayerId', this.curPlayerIdx );

        console.log("Running Story");
        console.log( "Current Player: " + this.curPlayerIdx );

        // Text
        let text_p1 = [];
        let text_p2 = [];

        let tags = [];

        while (this.engine.canContinue) {

            let nextText = this.engine.Continue();

            if (nextText.substring(0, 3) == "@1 ") {
                text_p1.push(nextText.substr(3));
            }
            else if (nextText.substring(0, 3) == "@2 ") {

                text_p2.push(nextText.substr(3));
            }
            else{
                text_p1.push(nextText);
                text_p2.push(nextText);
            }

            //collect tags at each point that can be continued
            this.engine.currentTags.forEach((tag) => {

                tags.push( tag );

            });
            
        }

        let overridePlayer = false;
        let images = [];

        tags.forEach((tag) => {

            console.log( "Tag: " + tag );

            let imageTag = 'image: ';
            if (tag.indexOf(imageTag) == 0) {
                images.push(tag.substring(imageTag.length));
            }

            let playerTag = 'player: ';
            if( tag.indexOf(playerTag) == 0 ){
                console.log("setting player to " + parseInt( tag.substring(playerTag.length), 10 ) );
                this.curPlayerIdx = parseInt( tag.substring(playerTag.length), 10 ) - 1;
                this.curPlayerIdx = this.curPlayerIdx % this.players.length;
            }

        });

        let image = '';
        if (images.length > 0) {
            image = images[Math.floor(Math.random() * images.length)];
        }

        // Choices
        this.choices = [];
        this.engine.currentChoices.forEach((choice) => {
            this.choices.push({
                index: choice.index,
                text: choice.text
            });
        });

        // Stats
        let stats = [];
        for (let stat in this.engine.variablesState._globalVariables) {
            if (stat.substring(0, 2) == "m_") {
                continue;
            }

            let value = getEngineStat(this.engine, stat);
            if (stat == 'TOD') {
                value = formatTimeStat(value);
            }
            stats.push({
                name: stat,
                value: value
            });
        }

        /*
        console.log(this.engine.currentTags);
        console.log(this.engine.variablesState.$("TOD"));
        */
        //console.log(this.engine.variablesState)

        //

        // if( !overridePlayer ){
        //     this.curPlayerIdx++;   
        // }

        // this.curPlayerIdx = this.curPlayerIdx % this.players.length;
        // setEngineStat(this.engine, 'currentPlayerID', this.curPlayerIdx );


        this.choiceTimer = 0;

        if (this.choices.length == 0) {
            console.log('Finished game', this.id);
            this.finished = true;
        }

        // Broadcast
        let choices = this.choices;
        let lastChoiceText = this.lastChoiceText;
        for (let i = 0; i < this.players.length; i++) {

            let text = ( i == 0 ? text_p1 : text_p2 );
            let p = this.players[i];
            p.sendMessage('updateStory', {
                playerId: this.players[this.curPlayerIdx].id,
                timeout: CHOICE_TIMEOUT,
                story: {
                    image,
                    text,
                    choices,
                    lastChoiceText,
                    stats
                }
            });
        }

        this.lastChoiceText = '';
        // Update local state

    }
}

//
// GameManager
//
class GameManager {
    constructor() {
        this.games = [];
        this.gameId = 0;
        this.prevTime = now();

        setInterval(() => { this.update(); }, 1000.0 / kTickRate);
    }

    onConnection(ws) {
        let game = null;
        if (this.games.length === 0 ||
            !this.games[this.games.length - 1].waitingForPlayers()) {
            game = new Game(this.gameId++);
            this.games.push(game);
            console.log('Created game', game.id);
        } else {
            game = this.games[this.games.length - 1];
        }

        game.onConnection(ws);
    }

    update() {
        let time = now();
        let dt = Math.min((time - this.prevTime) / 1000.0, 1.0 / 8.0);
        this.prevTime = time;

        for (let i = this.games.length - 1; i >= 0; i--) {
            let g = this.games[i];

            g.update(dt);

            if (g.isFinished()) {
                this.games.splice(i, 1);
            }
        }
    }
}

module.exports = {
    Game,
    GameManager
};
